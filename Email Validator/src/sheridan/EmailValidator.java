package sheridan;

public class EmailValidator {

	public static boolean isValidEmail(String email) {
		
		int atCount = 0; int atLocation = 0;
		int periodCount = 0; int periodLocation = 0;
		int accountLowerCaseCount = 0;
		int domainCharCount = 0;
		int extensionCharCount = 0;
		
		email = email.trim();
		
		if(email == "")
			return false;
		
		if(isNumeric(String.valueOf(email.charAt(0))))
			return false;
		
		for( int i = 0; i < email.length(); i++) {
			if(email.charAt(i) == '@' && atCount < 2) {
				atCount++;
				atLocation = i;
			}
			if(email.charAt(i) == '.' && periodCount < 2) {
				periodCount++;
				periodLocation = i;
			}
		}
		
		
		for (int i = 0; i < atLocation; i++) {
			if(Character.isLowerCase(email.charAt(i))){
				accountLowerCaseCount++;
			}
		}
		
		for(int i = atLocation; i < periodLocation; i++) {
			if(Character.isLowerCase(email.charAt(i)) || isNumeric(String.valueOf(email.charAt(i)))){
				domainCharCount++;
			}
		}
		
		for(int i = periodLocation; i < email.length(); i++) {
			if(Character.isLowerCase(email.charAt(i))){
				extensionCharCount++;
			}
		}
		
		if (atCount == 1 && periodCount == 1 && atLocation < periodLocation && accountLowerCaseCount >= 3 && domainCharCount >= 3 && extensionCharCount >= 2)
			return true;
		
		return false;
	}

	public static boolean isNumeric (String s) {
		try {
			Integer.parseInt(s);
			return true;
		} catch (NumberFormatException e ){
			return false;
		}
	}
	
}
