package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmailValidatorTest {

	@Test
	public void testIsValidEmail( ) {
		String email = "pellemas@sheridancollege.ca";
		assertTrue("Unable to validate the email", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testFormatRegular( ) {
		String email = "pellemas@sheridancollege.ca";
		assertTrue("Unable to validate the email", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testFormatException( ) {
		String email = "";
		assertFalse("Unable to validate the email", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testFormatBoundaryIn( ) {
		String email = "pel@she.ca";
		assertTrue("Unable to validate the email", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testFormatBoundaryOut( ) {
		String email = "pellemas.sheridancollege@ca";
		assertFalse("Unable to validate the email", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testAtSymbolRegular( ) {
		String email = "pellemas@sheridancollege.ca";
		assertTrue("Unable to validate the email", EmailValidator.isValidEmail(email));
	}
	@Test
	public void testAtSymbolException( ) {
		String email = "";
		assertFalse("Unable to validate the email", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testAtSymbolLowerBoundary( ) {
		String email = "pellemassheridancollege.ca";
		assertFalse("Unable to validate the email", EmailValidator.isValidEmail(email));
	}
	@Test
	public void testAtSymbolUpperboundary( ) {
		String email = "pellemas@sherid@ancollege.ca";
		assertFalse("Unable to validate the email", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testAccountNameException( ) {
		String email = "1pellemas@sherid@ancollege.ca";
		assertFalse("Unable to validate the email", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testAccountNameBoundaryIn( ) {
		String email = "pel1@sheridancollege.ca";
		assertTrue("Unable to validate the email", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testAccountNameBoundaryOut( ) {
		String email = "peLLE123@sheridancollege.ca";
		assertFalse("Unable to validate the email", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testDomainNameBoundaryIn( ) {
		String email = "pellemas@ab2.ca";
		assertTrue("Unable to validate the email", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testDomainNameBoundaryOut( ) {
		String email = "pellemas@a1.ca";
		assertFalse("Unable to validate the email", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testExtensionNameBoundaryIn( ) {
		String email = "pellemas@sheridancollege.ca";
		assertTrue("Unable to validate the email", EmailValidator.isValidEmail(email));
	}
	
	@Test
	public void testExtensionNameBoundaryOut( ) {
		String email = "pellemas@sheridancollege.c";
		assertFalse("Unable to validate the email", EmailValidator.isValidEmail(email));
	}
	
	
	

}
